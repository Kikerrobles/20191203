package fp.daw.examen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Ejercicio2 {

	/*
	 * 3 puntos
	 * 
	 * Escribir en el método main un programa que lea del teclado dos números enteros y calcule
	 * la suma de todos los números impares comprendidos entre ellos, ambos incluidos.
	 * 
	 * El programa deberá calcular el resultado sin importar el orden en que se introduzcan los dos números
	 * (primero el mayor o primero el menor).
	 * 
	 * Si es posible, minimizar el número de iteraciones que se ejecutarán para obener el resultado.
	 * 
	 * No se permite el uso de la clase Scanner para leer del teclado.
	 * 
	 * Sólo se permite el uso de recursos del lenguaje Java vistos en las unidades 2 y 3.
	 */
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		int numero1, numero2;
		int total = 0;
		
		System.out.print("Dime el primer número: ");
		numero1 = (int) Float.parseFloat(in.readLine());
		
		System.out.print("Dime el segundo número: ");
		numero2 = (int) Float.parseFloat(in.readLine());
		
		int numeroMasAlto = numero1 > numero2 ? numero1 : numero2;
		int numeroMasBajo = numero1 < numero2 ? numero1 : numero2;
		
		//Si es par le sumamos uno y asi podemos iterar cada dos numeros en vez de de uno en uno.
		//Tampoco es necesaria la comprobación de si es impar en cada iteración
		
		if(numeroMasBajo % 2 == 0)
			numeroMasBajo++;
		
		for(int i = numeroMasBajo; i <= numeroMasAlto; i+=2)
			total+=i;
		
		System.out.printf("Suma de los impares comprendidos entre %d-%d = %d", numeroMasBajo, numeroMasAlto, total);
		
	}

}
