package fp.daw.examen;

import java.util.Scanner;

public class Ejercicio1 {

	/* 
	 * 4 puntos
	 * 
	 * Escribir en el método main un programa que pida por teclado un número decimal menor o igual
	 * que 3000 y muestre por pantalla el resultado de convertirlo a un número romanos.
	 *     
	 *     La conversión se realizará transformando individualmente cada dígito, empezando por el correspondiente
	 *     a las unidades de millar, seguido de las centenas, decenas y unidades de la forma siguiente:
	 *     
	 *     		Dígito decimal		|	Transformación
	 *     		--------------------|----------------------------------------------------------------------------
	 *     		1, 2 o 3			|	Escribir C1 de una a tres veces
	 *     		4					|	Escribir C1 seguido de C2
	 *     		5, 6, 7 o 8			|	Escribir una vez C2 seguido de C1 de cero a tres veces
	 *     		9					|	Escribir C1 seguido de C3
	 *     
	 *     donde C1, C2 y C3 son los caracteres de numeración romana que corresponda según la tabla siguiente:
	 *     
	 *     							|   C1   |   C2   |   C3   |
	 *     		--------------------|--------|--------|--------|        
	 *     		unidades			|	I    |   V    |   X    |
	 *     		decenas				|	X    |   L    |   C    |
	 *     		centenas			|	C    |   D    |   M    |
	 *     		unidades de millar	|	M    |        |        |
	 *     
	 *     Sólo se permite el uso de recursos del lenguaje Java vistos en las unidades 2 y 3.
	 */

	public static void main(String[] args) {
		
		String numero, numeroEnRomanos = "", unidades = null;
		char[] caracteres;

		Scanner in = new Scanner(System.in);
		
		System.out.print("dime un numero entre 1 y 3000: ");
		numero = in.next();
		
		caracteres = numero.toCharArray();
		
		for(int i = 0;i < caracteres.length; i++) {
			switch(caracteres.length) {
				case 4 :
					if(i==0) { unidades = "millares"; break;}
					if(i==1) { unidades = "centenas"; break;}	
					if(i==2) { unidades = "decenas"; break;}
					if(i==3) { unidades = "unidades"; break;}
				case 3 :
					if(i==0) { unidades = "centenas"; break;}	
					if(i==1) { unidades = "decenas"; break;}
					if(i==2) { unidades = "unidades"; break;}
				case 2 :
					if(i==0) { unidades = "decenas"; break;}
					if(i==1) { unidades = "unidades"; break;}
				case 1 :
					if(i==0) { unidades = "unidades"; break;}
					
			}
			
			char c = caracteres[i]; 
			String cc = "" + c; 
			int valor = (new Integer(cc)).intValue();
			
			if(valor >= 5 && valor <= 8) {
				numeroEnRomanos += CadenasUnos(0,unidades,"cinco");
				numeroEnRomanos += CadenasUnos(valor-5, unidades, "unos");
			}
			if(valor >= 1 && valor <= 3) {
				numeroEnRomanos += CadenasUnos(valor, unidades, "unos");
			}
			else
				if(valor == 4)
					numeroEnRomanos += CadenasUnos(valor, unidades, "cuatro");
				else
					if(caracteres[i] == 9)
						numeroEnRomanos += CadenasUnos(valor, unidades, "nueve");
		}
		
		System.out.printf("numero %s", numeroEnRomanos);
		in.close();
		
	}
	
	static String CadenasUnos(int veces, String unidades, String tipo) {
		
		String cadena = "";
		String subcadena = "", subcadena2 = "", subcadena3 = "" ;
		
		switch(unidades) {
			case "unidades":
				subcadena = "I";
				subcadena2 = "V";
				subcadena3 = "X";
				break;
			case "decenas":
				subcadena = "X";
				subcadena2 = "L";
				subcadena3 = "C";
				break;
			case "centenas":
				subcadena = "C";
				subcadena2 = "D";
				subcadena3 = "M";
				break;
			case "millares":
				subcadena = "M";
				break;
		}
		
		switch(tipo) {
			case "unos":
			case "millares":
				for(int i = 1; i <= veces; i++)
					cadena += subcadena;
				break;
			case "cuatro":
				cadena = subcadena + subcadena2;
				break;
			case "cinco":
				cadena = subcadena2;
				break;
			case "nueve":
				cadena = subcadena + subcadena3;
				break;	
		}
		
		
		return cadena;
	}
	
}
